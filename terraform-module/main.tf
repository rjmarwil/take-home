# Modules used from https://github.com/terraform-aws-modules/terraform-aws-iam
provider "aws" {
  region = "us-east-1"
}

module "iam_assumable_role" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-assumable-role"
  version = "~> 4.3"

  trusted_role_arns = [
    "arn:aws:iam::835367859851:user/prod-ci-user",
  ]

  create_role = true

  role_name         = "prod-ci-role"
  role_requires_mfa = true

  custom_role_policy_arns = [
    "arn:aws:iam::aws:policy/prod-ci-policy",
  ]
  number_of_custom_role_policy_arns = 1
}

module "iam_policy" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-policy"
  version = "~> 4.3"

  name        = "prod-ci-policy"
  path        = "/"
  description = "Prod CI Policy"
  
  # Policy used from https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_use_passrole.html
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
          "iam:GetRole",
          "iam:PassRole"
      ],
      "Resource": "arn:aws:iam::account-id:role/prod-ci-role"
    }
  ]
}
EOF
}

module "iam_group_with_policies" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-group-with-policies"
  version = "~> 4.3"

  name = "prod-ci-group"

  group_users = [
    "prod-ci-user"
  ]

  attach_iam_self_management_policy = true

  custom_group_policy_arns = [
    "arn:aws:iam::aws:policy/prod-ci-policy",
  ]

  custom_group_policies = [
    {
      name   = "prod-ci-policy"
      policy = module.iam_policy.policy
    }
  ]
}

module "iam_user" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-user"
  version = "~> 4.3"

  name          = "prod-ci-user"
  force_destroy = true

  pgp_key = "keybase:test"

  password_reset_required = false
}