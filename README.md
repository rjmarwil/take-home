**Docker, kubernetes, CI/CD**
  1. `Dockerfile` which runs litecoin version 0.18.1
  2. `litecoin-statefulset.yaml` is a kubernetes statefulset that deploys the litecoin image from Dockerfile in 1.
  3. `.gitlab-ci.yml` is a simple Gitlab-CI build and deployment pipeline to build/run the files from 1. and 2.

  Instructions on how to run and further comments are included in each file listed above.

**Text Manipulation**

  SOURCE: 
  - The problem I chose is adapted from an interview question given to me before. My original answer used Ruby to solve it. I have never solved the problem using awk, sed, tr, grep, or Python.
  
  PROBLEM:
  - Given a log file (log.txt), find the total number of errors per service, as well as the count of errors for the service instance with the max number of errors. Print the output to the console.
  - The log file format follows: `TIMESTAMP [SERVICE INSTANCE_ID]: LOG_MESSAGE`
  - For example, given the `log.txt` file, the `api-gateway` service should show 13 errors, and the instance with the max errors should be instance `ffd3082fe09d`, with 11 out of the 13 errors.

  4. `text-manipulation.sh` solves the above text manipulation problem using awk and sed.
    
    - This file is written in Bash
    - Run script from the command line with `./text-manipulation.sh`

  5. `text-manipulation.py` solves the above text manipulation problem using Python.

    - This file is written in Python 3.9
    - Run script from the command line with `python3 text-manipulation.py`

**Terraform**

  6. In the `terraform-module` directory are `main.tf`, `outputs.tf`, and `variables.tf` files. They make up a Terraform module that creates the following resources in IAM:

    - A role, with no permissions, which can be assumed by users within the same account,
    - A policy, allowing users / entities to assume the above role,
    - A group, with the above policy attached,
    - A user, belonging to the above group.

  Test the module with the terraform cli using `terraform init` and then `terraform plan` to see what resources will be created.
