# This Dockerfile uses the litecoin-core docker image from hub.docker.com by uphold
#   - https://hub.docker.com/r/uphold/litecoin-core
# 
# Specific image used is https://hub.docker.com/layers/uphold/litecoin-core/0.18.1/images/sha256-fb3148b0ca9fb3075ea7670c1690d4fa8bcf323d72a4c7192d8da235d95ee994?context=explore
#   - Version 0.18.1
#   - Checksum verified sha256:fb3148b0ca9fb3075ea7670c1690d4fa8bcf323d72a4c7192d8da235d95ee994
# 
# Dockerfile for above image is https://github.com/uphold/docker-litecoin-core/blob/master/0.18/Dockerfile
#   - By default, litecoind will run as user litecoin for security reasons (https://github.com/uphold/docker-litecoin-core/blob/master/0.18/Dockerfile#L7)
# 
# Run either by:
#   1. Building the image locally:
#     $ docker build . -t litecoin:0.18.1
#     $ docker run --rm litecoin:0.18.1
#   2. Or by pulling remotely:
#     $ docker run --rm uphold/litecoin-core:0.18.1@sha256:fb3148b0ca9fb3075ea7670c1690d4fa8bcf323d72a4c7192d8da235d95ee994

FROM uphold/litecoin-core:0.18.1@sha256:fb3148b0ca9fb3075ea7670c1690d4fa8bcf323d72a4c7192d8da235d95ee994
