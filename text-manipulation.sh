#!/usr/bin/env bash

LOG_FILE="log.txt"
REGEX='Z \[(.*) (.*)\]: .* (\[error\]) '

parse_logs() {
  # Get total count of errors per service
    # STEPS:
    # Line 12 - Print service and instance if matches regex expression 'Z \[(.*) (.*)\]: .* (\[error\]) '
    # Line 13 - Use sed to remove '[', ']', and ':'
    # Line 14 - Count and remove duplicate services
  awk '/Z \[(.*) (.*)\]: .* (\[error\]) / { print $2, $3 }' $LOG_FILE \
  | sed 's/\[//;s/\]://' \
  | awk '{ service[$1]++ } END {for (error in service) print error, service[error] }' > service_errors.txt
  cat service_errors.txt
  # Get instance with max errors per service
    # STEPS:
    # Line 22 - Print service and instance if matches regex expression 'Z \[(.*) (.*)\]: .* (\[error\]) '
    # Line 23 - Use sed to remove '[', ']', and ':'
    # Line 24 - Count and remove duplicate instances (Taken from https://www.unix.com/shell-programming-and-scripting/247043-awk-count-using-each-unique-value.html)
    # Line 25 - Select row with max instance errors based on service (Taken from https://stackoverflow.com/questions/35069048/using-awk-to-get-the-maximum-value-of-a-column-for-each-unique-value-of-another)
  awk '/Z \[(.*) (.*)\]: .* (\[error\]) / { print $2, $3 }' $LOG_FILE \
  | sed 's/\[//;s/\]://' \
  | awk '{ service[$1 OFS $2]++ } END { for (instance in service) print instance, service[instance] }' \
  | awk '$1 > max[$3] { max[$3] = $1; row[$2] = $0 } END { for (i in row) print row[i]}' > instance_errors.txt
  cat instance_errors.txt
  # Merge files into single table
  # Sets the number column $2 from service_errors to hash referencing column $1 services, then print it into instance_errors based on matching service
  awk 'NR==FNR { service[$1] = $2; next } { OFS="\t"; print $1,service[$1],$2,$3 }' service_errors.txt instance_errors.txt > combined.txt
  
  # Pretty print to console with column headers and proper tabbings
  awk 'BEGIN { print "Service", "Service-Errors", "Max-Error-Instance", "Instance-Errors" } { print $0 }' combined.txt \
  | column -t

  rm service_errors.txt instance_errors.txt combined.txt
}

parse_logs
