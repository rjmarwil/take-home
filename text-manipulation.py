#!/usr/bin/python

import re

def main():
    log_file = "log.txt"
    regex = r'Z \[(.*) (.*)\]: .* (\[error\]) '

    data = parseData(log_file, regex)

    return data

def parseData(log_file, regex):
    dict = {}
    with open(log_file, "r") as file:
        data = file.readlines()

    for line in data:
        match = re.search(regex, line, flags=0)
        if match:
            service_name = match.group(1)
            instance_id = match.group(2)
            if service_name in dict.keys():
                if instance_id in dict[service_name].keys():
                    dict[service_name][instance_id] += 1
                else:
                    dict[service_name][instance_id] = 1
            else:
                dict[service_name] = {instance_id: 1}

    for service in dict:
        total_errors = sum(dict[service].values())
        max_instance = max(zip(dict[service].values(), dict[service].keys()))
        print(f'\nService Name: {service} - {total_errors} errors', flush=True)
        print(f'Instance With Max Errors: {max_instance[1]} - {max_instance[0]} / {total_errors} errors\n', flush=True)

    return dict

if __name__ == '__main__':
    main()
